class Board
  attr_reader :grid

  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  def self.default_grid
    Array.new(10) { Array.new(10) }
  end

  def display
    grid_hash = {:o=>"O",:x=>"X",:s=>"_",nil=>"_"}
    (0...@grid.length).each do |row|
      (0...@grid.length).each do |col|
        pos = [row,col]
        print "#{grid_hash[self[pos]]}"
      end
      print "\n"
    end
    print "\n"
  end

  def count
    @grid.flatten.count(:s)
  end

  def empty?(pos=nil)
    return self[pos]==nil unless pos.nil?
    @grid.flatten.all?(&:nil?)
  end

  def full?
    @grid.flatten.none?(&:nil?)
  end

  def place_random_ship
    raise "board full" if self.full?
    pos = [rand(@grid.length),rand(@grid.length)]
    until self.empty?(pos)
      pos = [rand(@grid.length),rand(@grid.length)]
    end
    self[pos]=:s
  end

  def won?
    @grid.flatten.count(:s) == 0
  end

  def populate_grid(num)
    (0...num).each do |i|
      self.place_random_ship
    end
  end

  def in_range?(pos)
    row,col = pos
    row < @grid.length || col < @grid.length
  end

  def [](pos)
    row,col = pos
    @grid[row][col]
  end

  def []=(pos,mark)
    row,col = pos
    @grid[row][col]=mark
  end

end
