class HumanPlayer

  def get_play
    print "\nEnter position to attack : row[space]col "
    pos = gets.chomp.split.map(&:to_i)
    until pos.length==2
      puts "\n\nNot enough coordinates, try again : "
      pos = gets.chomp.split.map(&:to_i)
    end
    pos
  end

end
