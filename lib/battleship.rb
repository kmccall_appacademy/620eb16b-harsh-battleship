require_relative "board.rb"
require_relative "player.rb"
require 'byebug'

class BattleshipGame

  attr_reader :board, :player

  def initialize(player,board)
    @player = player
    @board = board
  end

  def self.play_default
    player = HumanPlayer.new
    board = Board.new
    board.populate_grid(10)
    game = BattleshipGame.new(player,board)
    game.play
  end

  def play
    until self.game_over?
      print "\e[H\e[2J"
      self.board.display
      self.display_status
      self.play_turn
    end
    puts "Game Won !"
  end

  def play_turn
    pos = @player.get_play
    until @board.empty?(pos) || @board[pos]==:s
      puts "Position already attacked, try again : "
      pos = @player.get_play
    end
    self.attack(pos)
  end

  def validate_position(pos)
    @board.in_range?(pos)
  end

  def attack(pos)
    if @board[pos]==:s
      @board[pos]=:x
    else
      @board[pos]=:o
    end
  end

  def count
    @board.count
  end

  def game_over?
    @board.won?
  end

  def display_status
    print "\n\nNumber of Ships on board: #{self.count}"
    print "\nNumber of Ships destroyed: #{@board.grid.flatten.count(:x)}\n\n"
  end

end
